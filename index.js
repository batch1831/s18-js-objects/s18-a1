console.log("Hello World")

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"], 
	friends:{
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you!");
	}
}

console.log(trainer)

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

console.log("Result of talk method:");
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level*2;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);

		console.log(target.name+ "'s health is now reduced to " + (target.health - this.attack));

		target.health = target.health - this.attack;
		if (target.health - this.attack<=0){
			target.faint(target);
		}
	}

	this.faint = function(target){
		console.log(target.name + " has fainted.");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);





